<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        return view("services");
    }
    
    public function handwash()
    {
        return view("services.handwash");
    }

    public function barbershop()
    {
        return view("services.barber");
    }

    public function express()
    {
        return view("services.express");
    }

    public function mobilecarwash()
    {
        return view("services.mobile");
    }
    

}
