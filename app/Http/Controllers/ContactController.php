<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view("contact");
    }

    public function sendMail(Request $request){
        $email = $request->email;
        $subject = $request->subject;
        $message = $request->message;

        Mail::to('britney@movingprofessionals4u.com')->send(new SendMail($subject,$message));
        // britney@movingprofessionals4u.com
        return view("home");
    }
}
