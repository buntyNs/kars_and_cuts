<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('splash');
});

Route::get('/home', 'HomeController@index');
Route::get('/about-us', 'AboutController@index');
Route::get('/contact-us', 'ContactController@index');

Route::get('/services', 'ServiceController@index');

Route::get('/services/handwash', 'ServiceController@handwash');

Route::get('/services/barbershop', 'ServiceController@barbershop');

Route::get('/services/express', 'ServiceController@express');

Route::get('/services/mobilecarwash', 'ServiceController@mobilecarwash');

Route::post('/send-contact-mail', 'ContactController@sendMail');

