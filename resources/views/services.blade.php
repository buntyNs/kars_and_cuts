@extends("master")

@section("content")

<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-sv">
				<div class="opacity">
					<h1>Our Services</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Services</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service V1
			============================================== 
			-->
			<div class="service-version-one">
				<div class="container">
					<h2>We provide wide range of <br>Customer services.</h2>
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Hand Wash </a>
								
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Mobile Wash Unit</a>
								
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Express Detail</a>
								
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Complete Detail</a>
								


							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Interior Detail</a>
								


							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Exterior Detail</a>
								


							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Ozone Treatment</a>


							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<a href = "#"class="trans3s">Engine Detail</a>
								


							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->


						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<p>Barber Shop</p>
								
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->


					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.service-version-one -->


@endsection