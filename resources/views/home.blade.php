@extends("master")

@section("content")
					<section style = "margin-bottom:30px">
							<div class="container-fluid" style = "padding:0">
							<img src="images/home.jpg" alt="Image" class="wow fadeInLeft">

							</div>
					</section>

            <!-- 
			=============================================
				More About Us
			============================================== 
			-->
			<div class="more-about-us">
				<div class="image-box">
					<svg  version="1.1" class="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="854" height="632">
						<clipPath class="clip1">
							<use xlink:href="#shape-one" />
						</clipPath>
						<g clip-path="url(#shape-one)">
							<image width="854" height="632" href="images/about.jpeg" class="image-shape">
							</image>
						</g>
					</svg>
				</div>
				<div class="theme-shape-three"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-7 col-md-offset-5">
							<div class="main-content">
								<h2>Kars &amp; Cuts is the Club That providing first class auto reconditioning services &amp; Customer Services</h2>
								<div class="main-wrapper">
									<h4>Best Customer Services</h4>
									<p> We offer everything from hand car washes to complete showroom details from either one of our four convenient locations or one of our mobile wash units. Our professional, well trained auto groomers pay close attention to every specific need that your vehicle requires while recognizing that time is of the essence.</p>
									<img src="images/home/sign.png" alt="sign">
									<div class="button-wrapper p-bg-color">
										<span>Learn More</span>
										<a href="/about-us" class="hvr-icon-wobble-horizontal">More About us <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
									</div> <!-- /.button-wrapper -->
								</div> <!-- /.main-wrapper -->
							</div> <!-- /.main-content -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.more-about-us -->


            			<!-- 
			=============================================
				Home Service Section
			============================================== 
			-->
			<div class="home-service-section" style = "margin-top:200px">
				<div class="container">
					<div class="col-md-9 col-md-offset-3 main-container">
						<div class="theme-title">
							<h6>Our Services</h6>
							<h2>We provide wide range<br>Customer services.</h2>
							<p>We’ve strong  work history with different customer services</p>

						</div> <!-- /.theme-title -->
						<ul class="clearfix row">
							<li class="col-md-6">
								<div>
									<i class="flaticon-layers" style = "color:#4caf50"></i>
									<h5><a href="/services/residential" class="tran3s">Hand Wash</a></h5>
									<p>Fresh "Outside Only"</p>
								</div>
							</li>
							<li class="col-md-6">
								<div>
									<i class="flaticon-layers" style = "color:#2196f3"></i>
									<h5><a href="/services/commercial" class="tran3s">Barber Shop</a></h5>
									<p>Barber shop at its home base so you can stay looking fresh</p>
								</div>
							</li>
							<li class="col-md-6">
								<div>
									<i class="flaticon-layers" style = "color:#ff5722"></i>
									<h5><a href="services/packing" class="tran3s">Express Detail</a></h5>
									<p>Clean "Inside & Out"</p>
								</div>
							</li>
							<li class="col-md-6">
								<div>
									<i class="flaticon-layers" style = "color:#3f51b5"></i>
									<h5><a href="/services/senior" class="tran3s">Mobile Car Wash</a></h5>
									<p>clean your vehicle anytime</p>
								</div>
							</li>
						</ul>
					</div> <!-- /.main-container -->
					<img src="images/club.png" alt="Image" class="wow fadeInLeft">
				</div> <!-- /.container -->
			</div> <!-- /.home-service-section -->






			
			
@endsection