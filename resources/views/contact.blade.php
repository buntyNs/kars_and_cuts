@extends("master")

@section("content")


        <div class="inner-page-banner inner-page-banner-con">
				<div class="opacity">
					<h1>Contact US</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>About us</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->


            <div class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-xs-12">
                    <div class="contact-us-form">
                        <form method="post" action="{{url('send-contact-mail')}}" class="form-validation" id="contact" autocomplete="off">
                            {{ csrf_field() }}
                            <input class="require" type="email" placeholder="Email Address*" name="email">
                            <input class="require" type="text" placeholder="Subject*" name="subject">
                            <textarea class="require" placeholder="Your Message*" name="message"></textarea>
                            <button class="p-bg-color hvr-trim-two" type = >SEND MESSAGE</button>
                            <div id="result" style="color:green;"></div>
                        </form>
                    </div> <!-- /.contact-us-form -->
                </div> <!-- /.col- -->
                <div class="col-lg-5 col-md-6 col-xs-12">
                    <div class="contact-address">
                        <h2>Don’t Hesitate to contact with us for any kind of information</h2>
                        <p>Call us for imiditate support this number</p>
                        <a href="#" class="tran3s">(+94) 71 913 5668</a>
                        <ul>
                            <li><a href="https://www.facebook.com/avalancheLK" class="tran3s" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true" target="_blank"></i></a></li>
                            <li><a href="https://www.instagram.com/avalanche_lk/" class="tran3s" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/avalanchelk" class="tran3s" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true" target="_blank"></i></a></li>
                        </ul>
                    </div> <!-- /.contact-address -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.conatiner -->

        <!--Contact Form Validation Markup -->
        <!-- Contact alert -->
        <div class="alert-wrapper" id="alert-success">
            <div id="success">
                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="wrapper">
                    <p>Your message was sent successfully.</p>
                </div>
            </div>
        </div> <!-- End of .alert_wrapper -->
        <div class="alert-wrapper" id="alert-error">
            <div id="error">
                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="wrapper">
                    <p>Sorry!Something Went Wrong.</p>
                </div>
            </div>
        </div> <!-- End of .alert_wrapper -->
    </div>

            @endsection