@extends("master")

@section("content")

<div class="inner-page-banner inner-page-banner-ab">
				<div class="opacity">
					<h1>About Us</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>About us</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				About Text
			============================================== 
			-->


            <div class="company-seo-text" style = "margin-top : 100px">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-xs-12"><img src="/images/aboutus.jpg" alt=""></div>
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>Open 24 Hours a Day 7 Days a Week To Serve You</h2>
								</div>
								<p>
								No matter how big or small your fleet is Kars and Cuts can handle your business needs. We clean all types of vehicles, trucks, and heavy equipment, at your place or ours. When required Kars and Cuts also has the capability to reclaim its water used, to guarantee compliance with all local and state agencies.
								</p>
							</div>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
            </div> <!-- /.company-seo-text -->
            
       

@endsection