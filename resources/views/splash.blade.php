<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Kars and Cuts</title>
  
  
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css'>
<link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Architects+Daughter|Roboto&subset=latin,devanagari'>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

<link rel="stylesheet" href="/css/splash.css">
<link rel="icon" type="image/png" sizes="56x56" href="/images/logo.png">

  
</head>

<body>

  <body class="welcome">
  <span id="splash-overlay" class="splash"></span>
  <span id="welcome" class="z-depth-4"></span>
 
  <header class="navbar-fixed"> 
    <nav class="row red  nav-w">
       
      <div class="nav-wrapper">
          <a href="/" class="brand-logo" style="margin-left: 10px"><img width = "120px" src="/images/logo.png" alt="Logo"></a>
      </div>
    </nav>
  </header>

  <main class="valign-wrapper">
    <span class="container grey-text text-lighten-1 ">

      <p class="flow-text">Welcome to</p>
      <h1 class="title grey-text text-lighten-3">Kars and Cuts</h1>

      <!-- <blockquote class="flow-text">A place to study for your High School Equivalency Diploma</blockquote> -->
      <p class="flow-text">A place to
        <span
           class="txt-rotate"
           data-period="2000"
           data-rotate='[ "cut your hair.", "serivce your vehicles.", "get the loan for vehicles"]'></span>
      </p>

      <div>
        <!-- Dropdown Trigger -->
        
<a href= "/home" class="waves-effect waves-light btn red">Join Us</a>

      </div>

    </span>
  </main>


  <div id="message" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Contact</h4>
      <p>coming soon...</p>
    </div>
    <div class="modal-footer">
      <a href="" class="modal-action modal-close waves-effect btn-flat">close</a>
    </div> 
  </div> 

</body>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js'></script>
<script src="js/typing_carasole.js"></script>

  

</body>

</html>
