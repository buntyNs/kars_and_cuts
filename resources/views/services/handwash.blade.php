@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-hand">
				<div class="opacity">
					<h1>Hand Wash</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
					
						<div class="middle-text list-box-text wow fadeInUp">
							<h3>Outside Only</h3>
							<p></p>
							<ul>
								<li>
									<h4>Soft Lamb's Wool Mitt Hand Wash</h4>
									
								</li>
								<li>
									<h4>Complete Chamois Hand Dry</h4>
									
								</li>
							</ul>
						</div> <!-- /.middle-text -->


						<div class="middle-text list-box-text wow fadeInUp">
							<h3>Inside & Out</h3>
							<p></p>
							<ul>
								<li>
									<h4>Soft Lamb's Wool Mitt Hand Wash</h4>
									
								</li>
								<li>
									<h4>Complete Chamois Hand Dry</h4>
									
								</li>
								<li>
									<h4>Deluxe Wheel Wash & Tire Shine</h4>
									
								</li>
								<li>
									<h4>Interior/Exterior Windows</h4>
									
								</li>
								<li>
									<h4>Air Blow All Crevices to Prevent Water Stains</h4>
									
								</li>
								<li>
									<h4>Wipe Down Entire Interior</h4>
									
								</li>
								<li>
									<h4>Wipe Down All Door Jambs</h4>
									
								</li>
							</ul>
						</div> <!-- /.middle-text -->
					</div> <!-- /.box-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.service-details -->
			
			


@endsection