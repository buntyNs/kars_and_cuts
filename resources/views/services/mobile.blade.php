@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-mobile">
				<div class="opacity">
					<h1>Mobile Car Wash</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->


   
			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
						
						<div class="top-text">
							<h4>Kars and Cuts  will clean your vehicle anytime, at your location or ours. Whether at your home, office, parking lot, or favorite mall, we will be there any time, every time 24 hours a day. We also offer drop off and pick up service.</h4>
							<div class="row">
								<!-- <div class="col-md-6 wow fadeInLeft"><p>MP4U believes you deserve top notch treatment and nothing less.  When you choose MP4U, you receive a hard working crew of moving professionals trained to handle even the most fragile belongings. We always treat our customer’s items like they were our own and strive to give you the stress-free hassle-free relocation experience you deserve.</p></div>
								<div class="col-md-6 wow fadeInRight"><p>Call one of our knowledgeable and friendly moving managers today to learn more or schedule your free moving estimate by filling out our online form.</p></div> -->
							</div>
						</div> <!-- /.top-text -->

					</div> <!-- /.box-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.service-details -->
			
			


@endsection