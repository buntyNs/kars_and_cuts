@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-express">
				<div class="opacity">
					<h1>Express Detail</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
						<img src="images/home/13.jpg" alt="" class="feature-image">
						<div class="title clearfix">
							<h3 class="float-left">Fresh &  Clean Inside & Out</h3>
						</div> <!-- /.title -->
						
						<div class="middle-text list-box-text wow fadeInUp">
							<ul>
								<li>
									<h4>Complete Interior Vacuum</h4>
								</li>

								<li>
									<h4>Deluxe Carnauba Hand Wax</h4>
								</li>

								<li>
									<h4>Interior/Exterior Windows</h4>
								</li>

								<li>
									<h4>Tire Shine</h4>
								</li>

								<li>
									<h4> Complete Interior Cleaned & Conditioned</h4>
								</li>

								<li>
									<h4>All Door Jambs Cleaned</h4>
								</li>

								<li>
									<h4>Clean/Vacuum Trunk</h4>
								</li>
							</ul>
						</div> <!-- /.middle-text -->

						</div> <!-- /.bottom-text -->
					</div> <!-- /.box-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.service-details -->
			
			


@endsection