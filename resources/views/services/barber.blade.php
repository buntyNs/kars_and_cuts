@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-barber">
				<div class="opacity">
					<h1>Barber Shop</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
						<img src="images/home/13.jpg" alt="" class="feature-image">
						<div class="title clearfix">
							<h3 class="float-left">Barber Shop</h3>
						</div> <!-- /.title -->
						<div class="top-text">
							<h4>Kars and Cuts has a barber shop at its home base so you can stay looking fresh. Get your hair cut while your car is being washed, how convenient is that!</h4>
							<div class="row">
								<!-- <div class="col-md-6 wow fadeInLeft"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div>
								<div class="col-md-6 wow fadeInRight"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div> -->
							</div>
						</div> <!-- /.top-text -->

				<div class="container">
					<div class="row">
						<div class="col-md-6 col-xs-12"><img src="/images/barbershop.jpg" alt=""></div>
						<div class="col-md-6 col-xs-12">
							<div class="text">
								
								<p>
								There's a licensed barber on duty 7 days a week. Walk-ins are always welcome and accepted.
								</p>

								
							</div>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
				</div>
				</div>
		

@endsection